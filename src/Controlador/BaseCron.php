<?php
namespace App\Controlador;
use FMT\Controlador;

/**
 * Clase para manejar procesos cron.
 *
 * - ->debug() 				- sirve para debuguear en procesos hijos.
 * - ->getParams()			- Obtiene parametros pasados por enviromentes a procesos hijos
 * - ->ejecutar_accion()	- Ejecuta procesos hijos
*/
class BaseCron extends  \FMT\Controlador {
/**
 * Cantidad de proceso en simultaneo
 * @var        integer
 */
	const HIJOS							= 4;
/**
 * @var        array
 */
	protected static $PROCESOS_VIVOS	= [];
/**
 * Antes de ejecutar una accion, comprueba que sea llamado desde entorno servidor.
*/
	final public function procesar(){
		static::check_server_enviroment();
		parent::procesar();
	}
/**
 * Funcion para debugear los procesos que se ejecutan por sub-rutinas.
 * El resultado del debug se guarda en "/tmp/cron-debug".
 *
 * @param      mixing	$data   		- La informacion a debugear.
 * @param      boolean	$clean_debug	- Si es "true" limpia el archivo de registros anteriores. Default: false
 * @param	   string	$name			- Permite cambiar el nombre del archivo donde se guarda el archivo de error. Default: "cron-debug"
 *
 * @return void
 */
	final public function debug($data=null, $clean_debug=false, $name='cron-debug'){
		$trace	= debug_backtrace();
		$_debug	= print_r($data, true);

		$html	= <<<HTML

{$trace[0]['file']} | Linea: {$trace[0]['line']}
--------
{$_debug}
_______________________
HTML;

		if($clean_debug){
			$resource	= fopen("/tmp/{$name}.log", 'w+');
		} else {
			$resource	= fopen("/tmp/{$name}.log", 'a+');
		}
		fwrite($resource, $html);
		fclose($resource);
	}

/**
 * Comprueba que el metodo no se este llamando desde el navegador web. Solo proceso del sistema.
 */
	static final protected function check_server_enviroment(){
		$denegar	= ['REMOTE_ADDR', 'REQUEST_METHOD', 'HTTP_HOST', 'HTTP_CONNECTION'];
		foreach ($denegar as $value) {
			if(isset($_SERVER[$value])){
				header('HTTP/1.0 403 Forbidden ');
				exit;
			}
		}
		if(!(preg_match('/(cron.php)$/', $_SERVER['SCRIPT_NAME']))) {
			header('HTTP/1.0 403 Forbidden ');
			exit;
		}
	}

/**
 * Obtener parametros pasado como enviroments. Los parametros que recibe fueron pasados por "$this->ejecutar_accion()"
 * Se usa en procesos hijos.
 *
 * TODO: Contemplar el uso de  getopt()
 *
 * @return     Object|null
 */
	final protected function getParams(){
		if(!empty($_SERVER['argv'][2]) && !empty($_SERVER['argv'][3]) && ($_SERVER['argv'][2] == '--params' || $_SERVER['argv'][2] == '-p')){
			$fork_name	= $_SERVER['argv'][3];
			return unserialize(getenv($fork_name));
		}
		return null;
	}
/**
 * Controla que no se ejecuten mas procesos en simultaneo de los permitidos.
 * Antes de ejecutar un proceso, checkea que los procesos actuales en ejecucion esten vivos, caso contrario espera UN segundo, y vuelve a comprobar.
 *
 * @return void
*/
	private function preEjecucion(){
		usleep(100);
		while(count(static::$PROCESOS_VIVOS) >= static::HIJOS){
			foreach (static::$PROCESOS_VIVOS as $indice => &$resource) {
				if(is_resource($resource)){
					$data	= proc_get_status($resource);
					if(empty($data['running']) && empty(posix_getpgid($data['pid'] +1 ))){
						proc_close($resource);
						unset(static::$PROCESOS_VIVOS[$indice]);
						$resource	= null;
					}
				}
			}
			sleep(1);
		}
	}

/**
 * Agrega un proceso abierto por "proc_open" a la lista de procesos activos.
 *
 * @param Resource $resource
 * @return boolean
*/
	private function postEjecucion(&$resource=null){
		if(is_resource($resource)){
			static::$PROCESOS_VIVOS[]	= $resource;
			return true;
		}
		return false;
	}
/**
 * Ejecuta un proceso hijo. Los parametros son pasados como variables de entorno serializados.
 * Los errores de los hijos se muestran en "/tmp/cron-error-output.txt".
 *
 * @param      string	$accion     - Accion a ejecutar.
 * @param      mixing	$params		- Parametros  cualquiera.
 *
 * @return     boolean
 */
	final protected function ejecutar_accion($accion=null,$params=null){
		if(empty($accion) || empty($params)){
			return false;
		}
		$config	= Configuracion::instancia();
		if(!empty($config['app']['dev'])){
			$salida_stderr	= '/tmp/cron-error-output.log';
		} else {
			$salida_stderr	= '/dev/null';
		}
		$cwd		= __DIR__ . '/../../';
		$fork_name	= getmypid() . '_' . time();
		$enviroment	= [
			"{$fork_name}"	=> serialize($params)
		];

		$descriptorspec	= array(
			0	=> array("pipe", "w"),  // stdin es una tubería usada por el hijo para lectura
			1	=> array("pipe", "r"),  // stdout es una tubería usada por el hijo para escritura
			2	=> array("file", "/tmp/cron-error-output.log", "a") // stderr es un fichero para escritura
		);

		$this->preEjecucion();
		$child_process	= proc_open("php {$cwd}cron.php {$accion} --params {$fork_name} 2> {$salida_stderr} &", $descriptorspec, $pipes, '/tmp', $enviroment);
		return $this->postEjecucion($child_process);
	}

/**
 * Metodo utilizado para ejecutar procesos desde controladores comunes y silvestres.
 *
 * @param      string	$accion     - Accion a ejecutar.
 * @param      array	$params		- Parametros  cualquiera.
 *
 * @return     boolean
*/
	final static public function EjecutarAccion($accion=null,$params=null){
		if(empty($accion) || !is_array($params)){
			return false;
		}
		if(static::life_control()) {
			return (new self($accion))->ejecutar_accion($accion, $params);
		}
		return false;
	}

/**
 * Evalua si es posible ejecutar el proceso o si excede la cantidad de procesos permitido
 * @return bool
 */
	final static public function life_control(){
		$pids	= [];
		exec('pgrep -u www-data php -a | grep cron.php', $listado_procesos);
		foreach ($listado_procesos as $proceso) {
			preg_match('/^\d{4}?/', $proceso, $matches);
			$pids[] = $matches[0];
		}
		return !(count($pids) >= static::HIJOS);
	}

/**
 * Mata el proceso actual que lo llame.
*/
	final public function matarProceso(){
		posix_kill(posix_getpid(), SIGKILL);
	}
}