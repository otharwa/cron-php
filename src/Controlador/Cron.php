<?php
namespace App\Controlador;
use FMT\Logger;

/**
 * Acciones para procesar por cron del servidor.
*/
class Cron extends BaseCron {

/**
 * Test de proceso principal simple.
 * Se ejecuta con `sudo -u www-data php cron.php test_proceso`
*/
	public function accion_test_proceso(){
		$cron_process		= true;
		$params				= $this->getParams();
		$this->debug('accion_test_proceso', false); // /tmp/cron-debug.log por defecto
		$this->debug(time(), false);
		$test_data	= [
			'hola'	=> 'gerbacio',
		];
		// $this->ejecutar_accion('test_proceso_secundario', $test_data);
		static::EjecutarAccion('test_proceso_secundario', $test_data);
	}

/**
 * Test de proceso secundario. Posee control de tiempo de vida para evitar que se ejecute infinitamente.
*/
	public function accion_test_proceso_secundario($test_data=null){
		// static::EjecutarAccion('accion_test_proceso', $test_data);
		$cron_process	= false;
		if(!is_array($test_data)){
			$test_data	= $this->getParams();
			$cron_process	= true;
		}
		$this->debug('accion_test_proceso_secundario', false);
		$this->debug($test_data, false);
		$this->debug(time(), false);
		$this->matarProceso();
		$process_start	= time();
		while(true) {
			sleep(2);
			/** Realizar Harakiri luego de 12hs  */
			if(abs(time() - $process_start) >= abs(12*60*60)){
				$datos		= [
					'modelo'			=> 'Cron',
					'ejecucion'			=> ($cron_process ? 'sistema' : 'manual'),
					'parametros'		=> $cache_emails,
					'segundos_activo'	=> abs(time() - $process_start),
				];
				Logger::event('cron_informes_enviar_tiempo_agotado', $datos);
				$this->matarProceso();
			}

		}
	}
}